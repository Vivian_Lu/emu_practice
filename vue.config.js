const { defineConfig } = require('@vue/cli-service')
const packageJSON = require('./package.json')

const dotenv = require('dotenv')
dotenv.config()

process.env.VUE_APP_VERSION = packageJSON.version

module.exports = defineConfig({
  transpileDependencies: true,

  devServer: {
    host: process.env.VUE_APP_HOST_NAME,
    https: true,
    proxy: {
      '/api': {
        target: process.env.VUE_APP_API_URL,
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          '^/api': '',
        },
      },
      '/issue': {
        target: process.env.VUE_APP_SLACK_API_URL,
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          '^/issue': '',
        },
      },
    },
  },
})
