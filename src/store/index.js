import Vue from 'vue'
import Vuex from 'vuex'
import AuthResource from '@/api/restful/auth.js'

Vue.use(Vuex)

const auth = new AuthResource()

export default new Vuex.Store({
  state: {
    userIp: '',
  },
  mutations: {
    SET_USER_IP(state, ip) {
      state.userIp = ip
    },
  },
  actions: {
    async getUserIp({ commit }) {
      try {
        const ip = await auth.getUserIp()
        commit('SET_USER_IP', ip)
      } catch (error) {
        console.error('Error fetching user IP:', error)
      }
    },
  },
})
