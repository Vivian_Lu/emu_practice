import MainLayout from '@/layouts/MainLayout.vue'

const hallSubRouter = {
  path: '/accounts',
  name: 'accounts',
  title: '帳號管理',
  index: 'account',
  iconClass: 'el-icon-user',
  redirect: { name: 'hall-sub' },
  component: MainLayout,
  children: [
    {
      path: 'hall-sub',
      component: () => import('@/pages/hall-sub'),
      index: 'account-1',
      name: 'hall-sub',
      title: '站長子帳號',
    },
    {
      path: 'create',
      component: () => import('@/pages/hall-sub/components/HallSubCreate.vue'),
      name: 'hall-sub-create',
      title: '新增',
      meta: {
        hidden: true,
      },
    },
    {
      path: 'edit',
      component: () => import('@/pages/hall-sub/components/HallSubEdit.vue'),
      name: 'hall-sub-edit',
      title: '編輯',
      meta: {
        hidden: true,
      },
    },
  ],
}
export default hallSubRouter
