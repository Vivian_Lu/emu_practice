import Vue from 'vue'
import Router from 'vue-router'
import MainLayout from '@/layouts/MainLayout.vue'

import hallSubRouter from './modules/hall-sub'

Vue.use(Router)

export const constantRoutes = [
  {
    path: '/',
    component: MainLayout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/pages/redirect'),
      },
    ],
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/pages/login'),
  },
]

export const asyncRoutes = [hallSubRouter]

const router = new Router({
  mode: 'history',
  routes: [...constantRoutes, ...asyncRoutes],
})

export default router
