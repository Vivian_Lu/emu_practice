import InputText from './input/InputText.vue'
import InputDateRange from './input/InputDateRange.vue'
import InputSelect from './input/InputSelect.vue'
import BaseButton from './button/BaseButton.vue'
import SwitchButton from './button/SwitchButton.vue'
import ConfirmButton from './button/ConfirmButton.vue'
import CancelButton from './button/CancelButton.vue'
import MrcSearch from './basic/mrc-search'
import Pagination from './Pagination.vue'

export default {
  install(Vue) {
    Vue.component('InputText', InputText)
    Vue.component('InputDateRange', InputDateRange)
    Vue.component('InputSelect', InputSelect)
    Vue.component('BaseButton', BaseButton)
    Vue.component('SwitchButton', SwitchButton)
    Vue.component('ConfirmButton', ConfirmButton)
    Vue.component('CancelButton', CancelButton)
    Vue.component('MrcSearch', MrcSearch)
    Vue.component('Pagination', Pagination)
  },
}
