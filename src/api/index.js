import auth from './restful/auth'

export const exportBaseApiModules = () => {
  const modules = []
  modules.AuthResource = auth
  return modules
}
