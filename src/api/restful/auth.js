import Resource from './resource'
import request from '@/utils/request'
import store from '@/store'

class AuthResource extends Resource {
  constructor() {
    super('auth')
  }

  async getUserIp() {
    try {
      const response = await request({
        url: 'ping',
        method: 'get',
      })
      return response.data.Result.ip
    } catch (error) {
      console.error('Error fetching user IP:', error)
      return ''
    }
  }

  async login(params) {
    try {
      await store.dispatch('getUserIp')
      const userIp = store.state.userIp
      params.ip = userIp
    } catch (error) {
      console.error('Error fetching user IP in login:', error)
    }
    return await request({
      url: 'login',
      method: 'post',
      data: params,
    }).then((res) => res.data)
  }
}

export default AuthResource
