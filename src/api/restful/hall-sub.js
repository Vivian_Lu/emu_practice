import Resource from './resource'
import request from '@/utils/request'

class HallSubResource extends Resource {
  constructor() {
    super('hall_sub')
  }

  async getHallSubList(searchParams = {}) {
    const defaultParams = {
      is_blurry: 1,
      page: 1,
      count: 50,
      ...searchParams,
    }

    const apiToken = localStorage.getItem('apiToken')
    const headers = {
      apiToken: `${apiToken}`,
    }

    return await request({
      url: 'hall/subList',
      method: 'get',
      params: defaultParams,
      headers: headers,
    }).then((res) => res.data)
  }

  // async getAuthGroupDropdownList() {
  //   const defaultParams = {
  //     hall_code_goal: '',
  //     role_id: '',
  //   }

  //   const apiToken = localStorage.getItem('apiToken')
  //   const headers = {
  //     apiToken: `${apiToken}`,
  //   }

  //   return await request({
  //     url: 'newAuthGroup/dropdown',
  //     method: 'get',
  //     params: defaultParams,
  //     headers: headers,
  //   }).then((res) => res.data)
  // }
}

export default HallSubResource
