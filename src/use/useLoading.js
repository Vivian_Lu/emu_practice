import { ref, computed } from 'vue'

export const useLoading = () => {
  // 0 是 false
  const loading = ref(0)
  const isLoading = computed(() => !!loading.value)

  const unload = () => {
    loading.value--
  }

  const load = () => {
    loading.value++
  }

  return {
    unload,
    load,
    isLoading,
  }
}
