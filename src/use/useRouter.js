import { getCurrentInstance } from 'vue'

export const encodeRouterQuery = (value) =>
  encodeURIComponent(JSON.stringify(value))
export const decodeRouterQuery = (value) =>
  JSON.parse(decodeURIComponent(value))

export const useRouter = () => {
  const router = getCurrentInstance().proxy.$router
  const route = getCurrentInstance().proxy.$route

  return {
    router,
    route,
  }
}
