import { provide, nextTick } from 'vue'

export const registerPrefix = 'register:'
export const deregisterPrefix = 'deregister:'

const useProvideRegister = (event, parentFns) => {
  const childFns = []
  provide(registerPrefix + event, (fn) => {
    childFns.push(fn)
  })

  const provideHandler = () => {
    parentFns.forEach((fn) => fn())
    nextTick(() => {
      childFns.forEach((fn) => fn())
    })
  }

  provide(deregisterPrefix + event, (fn) => {
    const index = childFns.findIndex((fn2) => fn === fn2)
    if (index === -1) return
    childFns.splice(index, 1)
  })

  return provideHandler
}

export { useProvideRegister }
