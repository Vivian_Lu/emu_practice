export { default as AsideMenu } from './aside-menu/AsideMenu.vue'
export { default as HeaderBar } from './header-bar/HeaderBar.vue'
export { default as PageTabs } from './page-tabs/PageTabs.vue'
