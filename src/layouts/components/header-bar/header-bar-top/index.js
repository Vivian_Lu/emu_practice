export { default as StatisticPanel } from './StatisticPanel.vue'
export { default as DatePanel } from './DatePanel.vue'
export { default as LangSwitch } from './LangSwitch.vue'
