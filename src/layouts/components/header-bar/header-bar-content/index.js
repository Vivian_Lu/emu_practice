export { default as CustomerPanel } from './CustomerPanel.vue'
export { default as MailPanel } from './MailPanel.vue'
export { default as AudioPanel } from './AudioPanel.vue'
export { default as UserPanel } from './UserPanel.vue'
