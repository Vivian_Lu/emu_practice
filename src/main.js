import Vue from 'vue'
import App from '@/App.vue'

import router from '@/router'
import Components from '@/components'
import ElementUI from 'element-ui'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store'

import 'element-ui/lib/theme-chalk/index.css'
import '@mdi/font/css/materialdesignicons.min.css'

Vue.config.productionTip = false
Vue.use(Components)
Vue.use(ElementUI)
Vue.use(VueAxios, axios)

new Vue({
  render: (h) => h(App),

  router,
  store,
}).$mount('#app')
